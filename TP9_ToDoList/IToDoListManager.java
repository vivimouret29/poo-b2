import java.util.*;

public interface IToDoListManager
{
    /**
     * Crée une nouvelle ToDo liste et l'ajoute à la liste de ToDo listes existantes
     */
    void generateNewToDoList(String name);
    
    /**
     * Récupère l'ensemble des ToDo listes existantes
     */
    ArrayList<IToDoList> getAllToDoList();
    
    /**
     * Récupère une ToDo liste d'après sa position dans l'ensemble des ToDo listes existantes
     */
    IToDoList getToDoList(int position);
    
    /**
     * Supprime la liste à la position indiquée de l'ensemble des ToDo listes existantes.
     * Retourne true si la suppression a été effectuée avec succès. false si une erreur est survenue.
     */
    boolean removeToDoList(int position);

   /**
    * Méthode permettant de tester le manager.
    * Dans cette méthode, vous créez X listes, en supprimez Y etc...
    * Puis vous ajoutez des éléments dans chacune des listes, vous les cochez/décochez, etc...
    */
    void testToDoListManager();
}
