import java.util.*;

public abstract class ToDoListManager implements IToDoListManager 
{
    private ArrayList<IToDoList> AllList = new ArrayList<IToDoList>();
    
    /**
     * Crée une nouvelle ToDo liste et l'ajoute à la liste de ToDo listes existantes
     */
    public void generateNewToDoListManaged(String name)
    {
        generateNewToDoList("example");
        System.out.println("Ajout d'une liste");
    }
    
    /**
     * Récupère l'ensemble des ToDo listes existantes
     */
    public ArrayList<IToDoList> getAllToDoListManaged()
    {
        if (AllList.isEmpty())
            System.out.println("Listes vides");
        return AllList;
    }
    
    /**
     * Récupère une ToDo liste d'après sa position dans l'ensemble des ToDo listes existantes
     */
    public IToDoList getToDoListManaged(int position)
    {
        IToDoList liste = AllList.get(position);
        return liste;
    }
    
    /**
     * Supprime la liste à la position indiquée de l'ensemble des ToDo listes existantes.
     * Retourne true si la suppression a été effectuée avec succès. false si une erreur est survenue.
     */
    public boolean removeToDoListManaged(int position)
    {
        IToDoList liste = AllList.get(position);
        AllList.remove(liste);
        boolean remove = removeToDoList(position);
        if (!remove);
            System.out.println("Liste non supprimée");
        System.out.println("Liste supprimée");
        return remove;
    }    

    /**
    * Méthode permettant de tester le manager.
    * Dans cette méthode, vous créez X listes, en supprimez Y etc...
    * Puis vous ajoutez des éléments dans chacune des listes, vous les cochez/décochez, etc...
    */
    abstract public int testIToDoListManager();
}
