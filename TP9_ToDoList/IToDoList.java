import java.util.*;

public interface IToDoList
{
    boolean addItemToDo(ItemToDoList item);
    
    /**
     * Supprime un élément d'après sa position dans la liste et retourne true.
     * Si la position est incorrecte, on retourne false.
     */
    boolean removeItemToDo(int positionItem);
    
    /**
     * Vide la liste de tous les éléments qu'elle contient
     * On retourne true si tout est ok. False si un problème survient.
     */
    boolean removeAllItemsToDo();
    
    /**
     * Vide la liste de tous les éléments cochés qu'elle contient.
     * On retourne true si tout est ok. False si un problème survient.
     */
    boolean removeAllCheckedItems();
    
    /**
     * Coche un élément de la liste en se basant sur la position.
     * On retourne true si tout est ok. False si la position est incohérente ou l'item déjà coché.
     */
    boolean checkItemToDo(int positionItem);
    
    /**
     * Décoche un élément de la liste en se basant sur la position.
     * On retourne true si tout est ok. False si la position est incohérente ou l'item déjà décoché.
     */
    boolean uncheckItemToDo(int positionItem);
    
    /**
     * Retourne tous les éléments de la liste
     */
    ArrayList<ItemToDoList> getAllToDoListItems();
    
    /**
     * Retourne le nombre d'éléments dans la liste
     */
    int getToDoListSize();
    
    /**
     * Retourne le nom de la liste
     */
    String getName();
    
    /**
     * Défini le nom de la liste
     */
    void setName(String name);
    
}
