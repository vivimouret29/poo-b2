
/**
 * Write a description of class ItemToDoList here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ItemToDoList
{
    //Priorité de l'élément. Plus la valeur est petite, plus la tâche est prioritaire
    private int priorite;
    //Tâche à réaliser
    private String task;
    //Vrai si la tâche est traitée, false sinon
    private boolean checked;

    /**
     * Constructor for objects of class ItemToDoList
     */
    public ItemToDoList(int priorite, String task, boolean checked){
        this.priorite = priorite;
        this.task = task;
        this.checked = checked;
        
    }

    public int getPriorite(){
        return this.priorite;
    }
    
    public String getTask(){
        return this.task;
    }
    
    public boolean getChecked(){
        return this.checked;
    }
    
    public void setChecked(boolean checked){
        this.checked = checked;
    }
}
