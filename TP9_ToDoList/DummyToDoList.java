/**
 * Décrivez votre classe abstraite DummyToDoList ici.
 *
 * @author  (votre nom)
 * @version (un numéro de version ou une date)
 */
public abstract class DummyToDoList implements IToDoList
{
    // variable d'instance - remplacez cet exemple par le vôtre
    int x;
    int burger;
    /**
     * Un exemple de méthode concrète
     *
     * @param  y    le paramètre de cette méthode
     * @return        la somme de x et de y
     */
    public int exempleDeMethode(int y)
    {
        // Insérez votre code ici
        
        return x + y;
    }
    
    public String[] Menu(int burger)
    {
        // Insérez votre code ici
        String[] menu = {"Burger","Frites","Pizza","Nuggets","Eau"};
        return menu;
    }


    /**
     * Un exemple de méthode abstraite
     * @param  y    le paramètre de cette méthode
     * @return        qui sait ?
     */
    abstract public int exempleDeMethodAbstraite(int y);
}